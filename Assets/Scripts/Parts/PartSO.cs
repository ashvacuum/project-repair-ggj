﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace Interaction
{
    [CreateAssetMenu(menuName = "Part")]
    public class PartSO : ScriptableObject
    {
        public Sprite sprite;
        public TypesOfParts mode;
        [Header("For Items")]
        public List<TypesOfParts> Materials;


        [Header("For PlayerHand use")]
        public int PlayerID;

        void Start()
        {

            mode = new TypesOfParts();
            Materials = new List<TypesOfParts>(); ;

        }
        

        public void Clear()
        {
            mode.val = 0;
            sprite = null;
        }


        public  void Copy(PartSO ps)
        {
            sprite = ps.sprite;
            mode.val = ps.mode.val;
        }

        public void CopyMaterials(PartSO ps)
        {
            Materials.Clear();
            for (int i = 0; i < ps.Materials.Count; i++)
            {
                Materials.Add(ps.Materials[i]);
            }
        }
    }
}
