﻿using System.Collections;
using System.Collections.Generic;
using Interaction;
using Repair.Manager;
using UnityEngine;

public class PlayerControllers : MonoBehaviour
{

    //BOUND TO CAMERA VIEW
    private Vector2 screenBounds;
    private float objectWidth;
    private float objectHeight;

    private bool _canMove;
    
    public bool canMove { get{ return _canMove;}}

    
    
    //Movement
    public string HorizontalAxis = "Horizontal";
    public string VerticalAxis = "Vertical";

    public float moveSpeed = 2f;
    [SerializeField] float verticalMovement = 1f;
    [SerializeField] float horizontalMovement = 1f;

    // Animation
    public Animator animator;
    SpriteRenderer renderer;

    QTEManager manager;

    void Awake(){
        manager = GetComponent<QTEManager>();
        renderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();


    }

    void OnEnable(){
        manager.EventQTEStart += StopMove;
        manager.EventQTEEnd += AllowMove;
    }
    
    void OnDisable(){
        manager.EventQTEStart -= StopMove;
        manager.EventQTEEnd -= AllowMove;
    }

    void Start()
    {
        _canMove = true;
        //BOUND TO CAMERA VIEW
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        objectWidth = transform.GetComponent<SpriteRenderer>().bounds.extents.x; //extents = size of width / 2
        objectHeight = transform.GetComponent<SpriteRenderer>().bounds.extents.y; //extents = size of height / 2

        //Animation
        renderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
    }
    
    // Update is called once per frame
    void Update()
    {
        if(_canMove){
            Move();
        }
        
        // ANIMATIONS


    }

    private void Move(){
        //  BOUND TO CAMERA VIEW
        Vector3 viewPos = transform.position;
        viewPos.x = Mathf.Clamp(viewPos.x, screenBounds.x * -1 + objectWidth, screenBounds.x - objectWidth);
        viewPos.y = Mathf.Clamp(viewPos.y, screenBounds.y * -1 + objectHeight, screenBounds.y - objectHeight);
        transform.position = viewPos;



        horizontalMovement = Input.GetAxisRaw(HorizontalAxis);
        verticalMovement = Input.GetAxisRaw(VerticalAxis);

        //animator?.SetFloat("Speed", Mathf.Abs(horizontalMovement));
        if (horizontalMovement < 0)
            renderer.flipX = false;
        else if (horizontalMovement > 0)
            renderer.flipX = true;
        /*
        // UP DOWN FLIP PLACEHOLDER
        if (verticalMovement > 0)
            animator?.SetBool("goingUp", true);
        else
            animator?.SetBool("goingUp", false);


        if (verticalMovement < 0)
            animator?.SetBool("goingDown", true);
        else
            animator?.SetBool("goingDown", false);
*/
    }
    private void FixedUpdate()
    {
        transform.Translate(horizontalMovement * Time.fixedDeltaTime * moveSpeed, verticalMovement * Time.fixedDeltaTime * moveSpeed, 0);
    }

    private void StopMove(){
        _canMove = false;
    }

    private void AllowMove(GameObject a){
        _canMove = true;
    }
}
