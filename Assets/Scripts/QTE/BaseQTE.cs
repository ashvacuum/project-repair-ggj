﻿using System.Collections;
using System.Collections.Generic;
using Interaction;
using Repair.Manager;
using UnityEngine;
using UnityEngine.UI;

namespace Repair.QTE
{
    public abstract class BaseQTE : MonoBehaviour
    {

        [SerializeField]protected Image fillBar;
        [SerializeField]protected InteractBehaviour behaviour;

        protected virtual void Start(){
            
        }

        protected virtual void OnEnable(){

        }

        protected virtual void OnDisable(){
            
        }

        public void Initialize(GameObject objectToFollow){
            behaviour = GetComponent<InteractBehaviour>();
            Debug.Log(string.Format("{0} has been found: {1}", objectToFollow.name, behaviour != null));
        }

        protected virtual void Success(){
            GameManager.instance?.EndQTE();
        }
    }
}
