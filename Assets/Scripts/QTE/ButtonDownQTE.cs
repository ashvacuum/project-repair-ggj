﻿using System.Collections;
using System.Collections.Generic;
using Interaction;
using UnityEngine;
using UnityEngine.UI;

namespace Repair.QTE
{
    public class ButtonDownQTE : BaseQTE
    {
        [SerializeField]private int requiredButtonPresses = 10;

        private int actualPresses = 0;
        protected override void OnEnable(){
            fillBar.fillAmount = 0;
            actualPresses = 0;
        }

        void Update(){
            
            if(Input.GetKeyDown(behaviour.keyToPress) && actualPresses < requiredButtonPresses){
                actualPresses++;
                float loadValue = (float)actualPresses/(float)requiredButtonPresses;
                fillBar.fillAmount = loadValue;
            } else {
                //disable this object
                //reward item
                //enable player movement
            }

            
        }

        protected override void OnDisable(){
            
        }
    }

}
