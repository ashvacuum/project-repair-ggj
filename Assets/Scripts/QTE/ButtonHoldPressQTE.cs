﻿using System.Collections;
using System.Collections.Generic;
using Repair.QTE;
using UnityEngine;

namespace Repair.QTE
{ 
    public class ButtonHoldPressQTE : BaseQTE
    {
        [SerializeField]private float secondsToHold = 3;
        private float actualCurrentTime = 0;
        

        protected override void OnEnable(){
            actualCurrentTime = 0;
        }

        // Update is called once per frame
        void Update()
        {
            if(Input.GetKey(behaviour.keyToPress) && actualCurrentTime < secondsToHold){
                actualCurrentTime+= Time.deltaTime;
            } else {
                //disable
                //reward item
                //enable player movement
            }
        }
    }

}
