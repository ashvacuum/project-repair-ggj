﻿using System;
using System.Collections;
using System.Collections.Generic;
using Repair.QTE;
using UnityEngine;

namespace  Interaction
{
    public class QTEManager : MonoBehaviour
    {
        public PlayerControllers controllers;
        public float yOffset = 2f;
        public event Action EventQTEStart = () => {};
        public event Action<GameObject> EventQTEEnd = (mode) => {};

        public GameObject[] QTETypes;
        void Awake(){
            controllers= GetComponent<PlayerControllers>();
            EventQTEEnd += GiveItem;
        }

        void Update(){
            foreach(GameObject UI in QTETypes){

                UI.transform.position = Camera.main.WorldToScreenPoint(ComputeWithOffset());
            }
        }

        Vector3 ComputeWithOffset(){
            return new Vector3(this.transform.position.x, this.transform.position.y + yOffset, this.transform.position.z);
        }

        

        public void SpawnQTE(GameObject item){
            PartSO part;
            part = item.GetComponent<PartBehaviour>().partData;
            switch(part.mode.val){
                case TypesOfParts.Type.BATTERY:
                    EventQTEStart?.Invoke();
                    QTETypes[0].SetActive(true);
                    //SPAWN 0 qte
                    break;
                case TypesOfParts.Type.METAL:
                    EventQTEStart?.Invoke();
                    QTETypes[1].SetActive(true);
                    //spawn 1 qte
                    break;
                case TypesOfParts.Type.TAPE:
                    EventQTEStart?.Invoke();
                    QTETypes[2].SetActive(true);
                    //spawn 2 qte
                    break;
                default:
                    EventQTEEnd?.Invoke(item);
                    break;
            }
        }

        private void GiveItem(GameObject item){
            GetComponent<PickupStuffHandler>().CollectThis(item, true);

        }

        

    }
}
