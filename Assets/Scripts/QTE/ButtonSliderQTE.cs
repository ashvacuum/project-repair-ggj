﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Repair.QTE{
    public class ButtonSliderQTE : Repair.QTE.BaseQTE
    {
        [SerializeField]float speedMultiplier = 3;
        [SerializeField]float upperLimit = 0.3f;
        [SerializeField]float lowerLimit = 0.5f;

        bool goingUp;
        bool isOngoing;
        

        protected override void OnEnable(){
            fillBar.fillAmount= 0;
            goingUp = true;
            isOngoing = true;
        }

        protected override void OnDisable(){

        }

        // Update is called once per frame
        void Update()
        {
            if(!isOngoing) return;
            MoveFillbar();
            StopFillbar();
        }

        void MoveFillbar(){
            if(fillBar.fillAmount > 0.1f){
                fillBar.fillAmount += Time.deltaTime * speedMultiplier;
            } else if(fillBar.fillAmount > 0.9f){
                fillBar.fillAmount -= Time.deltaTime * speedMultiplier;
            }
        }

        void StopFillbar(){
            if(Input.GetKey(behaviour.keyToPress)){
                isOngoing = false;
                if(fillBar.fillAmount <= upperLimit && fillBar.fillAmount >= lowerLimit){
                    //reward player
                    //disable QTE
                    //
                } else {
                    isOngoing = true;
                }
            }
        }
    }
}
