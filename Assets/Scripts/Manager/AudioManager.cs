﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Repair.Audio;

namespace  Repair.Manager
{       
    [RequireComponent(typeof(AudioAsset))]
    public class AudioManager : MonoBehaviour
    {
        private AudioAsset _asset;

        public static AudioManager instance { get; private set;}

        void Awake(){
            if(instance == null){
                instance = this;
            } else {
                Destroy(this);
            }
        }

        void Start(){
            _asset = GetComponent<AudioAsset>();
        }

        public void PlayMusic(string name){
            _asset.PlayMusic(name);
        }
        
    }
}
