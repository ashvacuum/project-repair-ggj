﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace Repair.Manager {
    public class GameManager : MonoBehaviour
    {
        public static GameManager instance { get; private set;}

        #region Game Events

        public static event Action EventGameStart = () => {};
        public static event Action EventLevelEnd = () => {};
        public static event Action EventGamePause = () => {};
        public static event Action<int> EventCustomerFix = (score) => {};
        public static event Action EventCustomerTimeOut = () => {};
        public static event Action EventGameEnd = () => {};

        public static event Action EventTriggerQTE = () => {};
        public static event Action EventQTEFinish = () => {};

        public static event Action<GameObject> EventPlayerInteract = (player) => {};

        #endregion

        void Awake(){
            if(instance == null){
                instance = this;
            } else {
                Destroy(this);
            }
        }

        public void EndGame(){
            EventGameEnd?.Invoke();
        }

        public void StartGame(){
            EventGameStart?.Invoke();
        }

        public void PauseGame(){
            EventGamePause?.Invoke();
        }

        public void OnItemFixed(){
            EventCustomerFix?.Invoke(1);
        }

        public void StartQTE(){
            EventTriggerQTE?.Invoke();
        }

        public void EndQTE(){
            EventQTEFinish?.Invoke();
        }
    }
}
