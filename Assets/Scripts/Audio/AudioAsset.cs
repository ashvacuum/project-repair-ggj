﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Repair.Audio {
    public enum AudioStyle {
        BGM,
        SFX
    }

    [System.Serializable]
    public struct AudioType {
        public string name { get { return _name;}}
        public AudioClip clip{ get { return _clip;}}
        public AudioStyle make{ get { return _make;}}

        [SerializeField]private string _name;
        [SerializeField]private AudioClip _clip;
        [SerializeField]private AudioStyle _make;
    }

    public class AudioAsset : MonoBehaviour
    {
        [SerializeField] private AudioSource _bgm = null;
        [SerializeField] private AudioSource _sfx = null;

        [SerializeField]private List<AudioType> _audioList = new List<AudioType>();

        public void PlayMusic(string name){
            foreach(AudioType song in _audioList){
                if(song.name == name){
                    if(song.make == AudioStyle.SFX){
                        PlaySFX(song.clip);
                        return;
                    } else {
                        PlayBGM(song.clip);
                        return;
                    }
                }
            }
            Debug.Log(string.Format("{0} does not exist in the list", name));
        }

        private void PlaySFX(AudioClip clip){
            _sfx.PlayOneShot(clip);
        }

        private void PlayBGM(AudioClip clip){
            _bgm.clip = clip;
            _bgm.loop = true;
            _bgm.Play();
        }

    }
}
