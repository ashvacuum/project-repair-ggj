﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Repair.Manager;

namespace Interaction
{
    public class Interact : MonoBehaviour
    {
        [SerializeField] string InteractionBtn = "Submit";
        public UnityEvent OnInteract = new UnityEvent();
        private InteractBehaviour currentPlayer;

        [SerializeField]private GameObject part;

        [SerializeField]private PickupPart partToGive;

        void Awake(){
            partToGive = GetComponent<PickupPart>();
            currentPlayer = null;
            OnInteract.RemoveAllListeners();
        }
        void Start()
        {
            if(partToGive.part != null)
                part = partToGive.part;
        }

        public void OnEnable(){
            
        }
        
        void Update()
        {
            if(currentPlayer == null) return;
            if (Input.GetButtonDown(currentPlayer.InteractionBtn))
            {
                currentPlayer.GetComponent<QTEManager>().SpawnQTE(part);
                //GameManager.instance?.StartQTE();
                //OnInteract?.Invoke();
            }
        }

        public void AddPlayer(GameObject player){
            currentPlayer =  player.GetComponent<InteractBehaviour>();
            
        }

        public void RemovePlayer(){
            currentPlayer = null;
        }

        private void InteractPressed(){
            OnInteract?.Invoke();
        }
    }

}

