﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movment : MonoBehaviour
{

    public string HorizontalAxis = "Horizontal";
    public string VerticalAxis = "Vertical";

    public float moveSpeed = 2f;
    [SerializeField] float verticalMovement = 1f;
    [SerializeField] float horizontalMovement = 1f;

    void Update()
    {
        horizontalMovement = Input.GetAxis(HorizontalAxis);
        verticalMovement = Input.GetAxis(VerticalAxis);


    }
    private void FixedUpdate()
    {
        transform.Translate(horizontalMovement * Time.fixedDeltaTime * moveSpeed, verticalMovement * Time.fixedDeltaTime * moveSpeed, 0);
    }
}
