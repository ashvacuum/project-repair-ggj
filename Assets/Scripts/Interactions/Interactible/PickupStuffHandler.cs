﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Repair.Manager;

namespace Interaction
{
    [RequireComponent(typeof(InteractBehaviour))]
    public class PickupStuffHandler : MonoBehaviour
    {
        GameObject ItemOnHead;
        public Transform WorkBench;
        InteractBehaviour ib;
        public Transform HoldPosition;
        public PartSO PlayerHand;

        
        private void Start()
        {
            ib = GetComponent<InteractBehaviour>();
            PlayerHand.Clear();
        }

        public void CollectThis(GameObject part,bool isCopy)
        {
            GameManager.instance?.EndQTE();
            if (HoldPosition.childCount < 1)
            {
                if(isCopy)
                {
                    GameObject go = Instantiate(part, HoldPosition);
                    go.transform.localPosition = Vector3.zero;
                    PlayerHand.Copy(part.GetComponent<PartBehaviour>().partData);

                }
                else
                {
                    part.transform.SetParent(HoldPosition);
                    part.transform.localPosition = Vector3.zero;
                    PlayerHand.Copy(part.GetComponent<PartBehaviour>().partData);

                }
                
            }
            

            //parent for now but we can just change the fooking sprite later 
            //mustchange clone too
        }

        public void DropThis(bool throwAway)
        {
            if (HoldPosition.childCount >= 1)
            {
                if(throwAway)
                {
                    Destroy(HoldPosition.GetChild(0).gameObject);
                    PlayerHand.Clear();
                }
                else
                { 
                    Transform t = HoldPosition.GetChild(0);
                    t.SetParent(WorkBench);
                    t.localPosition = Vector3.zero;

                    PlayerHand.Clear();
                }
             

                //HoldPosition.GetChild(0).gameObject.SetActive(false);
                //HoldPosition.transform.DetachChildren();


                ///poooool
            }
                                
        }

        public void UseThis()
        {
            if (HoldPosition.childCount >= 1)
            {
                Destroy(HoldPosition.GetChild(0).gameObject);
                PlayerHand.Clear();            

            }

        }





    }
}
