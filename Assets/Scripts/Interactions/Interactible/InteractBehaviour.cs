﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace Interaction
{
    public class InteractBehaviour : MonoBehaviour
    {
        public string InteractionBtn = "Interact";
        public KeyCode keyToPress;
        public delegate void  OnInteract();
        public OnInteract onInteract;
        
        public event Action EventPlayerInteractStart = () => {};
        public event Action EventPlayerInteractEnd = () => {};
        private bool isBeingInteracted;
        

        void Start()
        {
            isBeingInteracted = false;
            onInteract = null;
          //  onInteract += dostuff;
        }

        //public void dostuff()
        //{
        //    Debug.Log("Dostuff");
        //}

      
        void Update()
        {
            Interact();
        }

        void Interact(){
            if(Input.GetButtonDown(InteractionBtn) && !isBeingInteracted)
            {
                onInteract?.Invoke();
            }
        }
    }


}
