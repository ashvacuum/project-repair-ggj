﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Interaction
{
    public class DropPart : MonoBehaviour
    {
        public UnityEvent OnItemPlaced = new UnityEvent();
         bool canPlace = true;

        public void setCanPlace(bool val)
        {
            canPlace = val;
        }
        public void DropPartOnHand()
        {
            if (GetComponent<CollisionDetection>().objectColliding.GetComponent<PickupStuffHandler>().PlayerHand.mode.val == TypesOfParts.Type.ITEM)
            {
                return;
            }
            else
            {
                GetComponent<CollisionDetection>().objectColliding.GetComponent<PickupStuffHandler>().DropThis(true);
            }
           
              
        }

        public void PlaceItem(Transform workbench)
        {
            if (!canPlace) return;
            PickupStuffHandler p = GetComponent<CollisionDetection>().objectColliding.GetComponent<PickupStuffHandler>();
            if (p.PlayerHand.mode.val == TypesOfParts.Type.ITEM)
            {
                p.WorkBench = workbench;
                p.DropThis(false);
                
                if (OnItemPlaced != null) OnItemPlaced.Invoke();
               
            }
            else
            {
                return;
            }


        }

        



    }


}
