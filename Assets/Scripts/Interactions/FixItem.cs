﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Interaction;
using UnityEngine.Events;
using Repair.Manager;

namespace Interaction.Repair
{
    public class FixItem : MonoBehaviour
    {
        public GameObject CurrentItemUnderRepair;
        [SerializeField]
        PartSO TargetItem;
        [SerializeField]
        PartSO PlayerHand;

        [SerializeField]
        List<TypesOfParts> PartsAdded;
        [SerializeField]
        List<TypesOfParts> PartsNeeded;
        [SerializeField] int curFixPoints = 0;
        [SerializeField] int targetFixPoints = 0;

        bool fixable = false;

        public UnityEvent OnItemFix = new UnityEvent();
        void Start()
        {
            //TargetItem = new PartSO(); 
            //PlayerHand = new PartSO();
            PartsAdded = new List<TypesOfParts>();
            PartsNeeded = new List<TypesOfParts>();
            
        }

        public void SetFixable(bool val)
        {
            fixable = val;
        }
        public void SetItem(GameObject thingy)
        {
            CurrentItemUnderRepair = thingy;
        }
        void SetFixPoints()
        {
            
            targetFixPoints = TargetItem.Materials.Count;
            curFixPoints = 0; 

        }

        public void AddFixPoints()
        {
            
            curFixPoints += 1;
            if(curFixPoints >= targetFixPoints)
            {
                Debug.Log("Item Complete");
                OnItemFix?.Invoke();
                //GameManager.EventCustomerFix
            }
        }

        public void SetPlayer(PartSO _playerHand)
        {
            PlayerHand = _playerHand;
            checkIfCorrectPart(_playerHand.mode);
        }
         
        public void InitializeItem()
        {
            PartsNeeded.Clear();

            for (int i = 0; i < TargetItem.Materials.Count; i++)
            {
                PartsNeeded.Add(TargetItem.Materials[i]);
            }

            PartsAdded.Clear();
            SetFixPoints();


        }

        public void checkIfCorrectPart(TypesOfParts _type)
        {
            for( int i = 0; i < PartsNeeded.Count; i++ )
            {
               
                if (PartsNeeded[i].val == _type.val)
                {
                  
                    //PartsAdded.Add(_type);
                    PartsNeeded.RemoveAt(i);
                    AddFixPoints();
                    return;
                }

            }
            return;
        }

    }
}
    

