﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Interaction
{
    [RequireComponent(typeof(SpriteRenderer))]

    public class ItemBehaviour : MonoBehaviour
    {
        public PartSO ItemData;
        SpriteRenderer sprite;

        void Start()
        {
            if (ItemData == null)
            {
                Debug.LogError("No part data");
            }

            InitializePart();

        }

        void InitializePart()
        {
            sprite = GetComponent<SpriteRenderer>();
            sprite.sprite = ItemData.sprite;
        }

    }

}
