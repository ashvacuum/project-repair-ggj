﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Interaction;

[RequireComponent(typeof(SpriteRenderer))]
public class PartBehaviour : MonoBehaviour
{
    public PartSO partData;
    SpriteRenderer sprite;
   
    void Start()
    {
        if(partData == null)
        {
            Debug.LogError("No part data");
        }

        InitializePart();

    }

    void InitializePart()
    {
        sprite = GetComponent<SpriteRenderer>();
        sprite.sprite = partData.sprite;
    }

   
}
