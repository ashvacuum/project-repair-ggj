﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


namespace Interaction
{
    public class WorkbenchInteraction : MonoBehaviour
    {
        public TypesOfParts type = new TypesOfParts();    


        public List<UnityEvent> EventSet = new List<UnityEvent>();
      
        public void Interact()
        { 

                        
            for (int i = 0; i < EventSet.Count; i++)
            { 
                if (EventSet[i] != null)
                    EventSet[i].Invoke();
            }
        }
    }
}

