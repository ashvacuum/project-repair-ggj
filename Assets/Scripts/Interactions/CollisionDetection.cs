﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Repair.Manager;

namespace Interaction
{

    [RequireComponent(typeof(BoxCollider2D))]
    
    public class CollisionDetection : MonoBehaviour
    {
        private GameObject currentPlayer;
        private Interact _interactable;
        BoxCollider2D trigger;
        public UnityEvent OnEnter = new UnityEvent();
        public UnityEvent OnExit = new UnityEvent();
        public string Target = "Player";
        [HideInInspector]
        public Collider2D objectColliding;

        bool canCollide = true;

        bool hasPlayer;

        void Start(){
            hasPlayer = false;
            _interactable = GetComponent<Interact>();
        }

        public void SetCanCollide(bool val)
        {
            canCollide = val;
        }
        void OnTriggerEnter2D(Collider2D col)
        {
            if (!canCollide) return;
           
            if (col.tag == Target && !hasPlayer)
            {
                hasPlayer = true;
                currentPlayer = col.gameObject;
                
                objectColliding = col;
                _interactable.AddPlayer(col.gameObject);
                if (OnEnter != null) OnEnter.Invoke();
               
            }
        }

        void OnTriggerExit2D(Collider2D col)
        {
            if (!canCollide) return;
            if (col.tag == Target && col.gameObject == currentPlayer)
            {
                hasPlayer = false;
                currentPlayer = null;
                objectColliding = null;
                _interactable.RemovePlayer();
                if (OnExit != null) OnExit.Invoke();
            }
            
        }
    }
}
