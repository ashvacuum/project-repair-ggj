﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


namespace Interaction
{

    [RequireComponent(typeof(CollisionDetection))]
    public class PickupPart : MonoBehaviour
    {
        public GameObject part;
        public UnityEvent OnPickup = new UnityEvent(); 
        public void CHANGE_PART(GameObject _part)
        {
            part = _part;
        }
        public void CollectPart(GameObject part)
        {
            GetComponent<CollisionDetection>().objectColliding.GetComponent<PickupStuffHandler>().CollectThis(part, true);
        
            if (OnPickup != null) OnPickup.Invoke();

            //for cleanup
        }

        public void CollectItem(GameObject part)
        {
            if (part == null) return;
            GetComponent<CollisionDetection>().objectColliding.GetComponent<PickupStuffHandler>().CollectThis(part,false);
            part = null;
            if (OnPickup != null) OnPickup.Invoke();
            //for cleanup
        }
    }

}
