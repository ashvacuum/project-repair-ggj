﻿using UnityEngine;
using UnityEngine.Events;
using TMPro;


namespace Interaction
{
    [DisallowMultipleComponent]
    public class Timer : MonoBehaviour
    {
        [SerializeField] float duration;
        [Space(25)]
        public UnityEvent onInvoke;
        public TextMeshProUGUI counterText;

        float currentTime;

        void Start()
        {
            currentTime = duration;
        }

        void OnEnable()
        {
            currentTime = duration;
        }


        void Update()
        {
            currentTime -= Time.deltaTime;

            if (counterText != null) counterText.text = currentTime.ToString("N0");



            if (currentTime < 0)
            {
                currentTime = duration;
                HandleOnRaise();
            }
        }

        void HandleOnRaise()
        {
            if (onInvoke != null)
            {
                onInvoke.Invoke();
            }
        }
    }
}
