﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Interaction.Repair;
using UnityEngine.Events;
namespace Interaction
{

    [RequireComponent(typeof(CollisionDetection))]
    public class UseThisPart : MonoBehaviour
    {
        // public GameObject part;
        bool canfix = false;
        PickupStuffHandler p;
       [Header("insert qte here")]
        public UnityEvent OnRepair = new UnityEvent();
        void Start()
        {
            p = new PickupStuffHandler();
        }

        public void SetCanFix(bool val)
        {
            canfix = val;
        }

        public void ActivateQTE()
        {
            if (!canfix) return;

             p = GetComponent<CollisionDetection>().objectColliding.GetComponent<PickupStuffHandler>();

            GetComponent<FixItem>().SetPlayer(p.PlayerHand);
         //   GetComponent<FixItem>().SetItem(GetComponent<CollisionDetection>().objectColliding.gameObject);

            if (p.PlayerHand.mode.val == TypesOfParts.Type.HAND || p.PlayerHand.mode.val == TypesOfParts.Type.ITEM) return;
            p.UseThis();

            if (OnRepair != null)
            {
                OnRepair.Invoke();
            }
           

        }

        public void UsePart()
        {
                    p.UseThis();

        }

    }
}
